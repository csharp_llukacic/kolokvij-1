﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp1 {
    sealed class Korisnik {
        //Konstruktori
        private Korisnik(string KorisničkoIme, string ImePrezime, int JedinstveniBroj, bool MikrofonKamera) {
            this.User = KorisničkoIme;
            this.Name = ImePrezime;
            this.ID = JedinstveniBroj;
            this.MicCam = MikrofonKamera;

        }
        //Getteri, setteri
        private string User { get; set; }
        private string Name { get; set; }
        private int ID { get; set; }
        private bool MicCam { get; set; }

        private string ToString() { }

        }
    }
}
